var counter = 0;
$(document).ready(initialise);

function initialise() {
    loadBuku();
    countStar(id);
}

function loadBuku(){
	$.ajax({
		url: "/buku/",
		success: function(result){
			result = result.items
			var hdr = "<thead><tr><th>Judul</th><th>Penulis</th><th>Cover</th><th>Deskripsi</th><th></th></tr></thead>";
				$("#btable").append(hdr);
				$("#btable").append("<tbody>");
			for(i=0; i<result.length; i++){
				var tmp = "<tr><td>" + result[i].volumeInfo.title + "</td><td>" + result[i].volumeInfo.authors + "</td><td>" +"<img src='"+result[i].volumeInfo.imageLinks.thumbnail+ "'>" + "</td><td>" + result[i].volumeInfo.description + "</td><td>" + "<button class='button' style = 'background-color: Transparent; border: none' id='"+result[i].id+"' onclick = 'countStar("+ "\""+result[i].id+"\"" +")'> <i class='fa fa-star'style = 'color : gray'></i></button>"+"</td></tr>";
				$("#btable").append(tmp);
			}
			$("#btable").append("</tbody>");
		}
	});
}

function countStar(id){
	var star = $('#'+id).html();
	if(star.includes("gray")) {
		counter++;
		$('#'+id).html("<i class='fa fa-star' style = 'color : blue'></i>");
		$("#checkFav").html("<i class='fa fa-star'style = 'color : blue'></i> " +counter + " Buku favorit");
	}
	else{
		counter--;
		$('#'+id).html("<i class='fa fa-star' style = 'color : gray'></i>");
		if(counter == 0){
			$("#checkFav").html("<i class='fa fa-star'style = 'color : blue'></i>  Favorit saya");
		}else{
		$("#checkFav").html("<i class='fa fa-star'style = 'color : blue'></i> " +counter + " Buku favorit");
		}
	}	
}
var counter = 0;
		function changeStar(id){
			var star = $('#'+id).html();
			if(star.includes("gray")) {
				counter++;
				$('#'+id).html("<i class='fa fa-star' style = 'color : orange'></i>");
				$("#statusBuku").html("<i class='fa fa-star'style = 'color : orange'></i> " +counter + " buku favorit");
			}
			else{
				counter--;
				$('#'+id).html("<i class='fa fa-star' style = 'color : gray'></i>");
				$("#statusBuku").html("<i class='fa fa-star'style = 'color : orange'></i> " +counter + " buku favorit");
			}	
		}
var dataLength = 0;
		$(document).ready(function(){
			$.ajax({
				url: "/buku/",
				success: function(result){
					result = result.items
					var hdr = "<thead><tr><th>Judul</th><th>Penulis</th><th>Sampul</th><th>Deskripsi</th><th></th></tr></thead>";
						$("#shelf").append(hdr);
						$("#shelf").append("<tbody>");
						dataLength = result.length;
					for(i=0; i<result.length; i++){
						var tmp = "<tr><td>" + result[i].volumeInfo.title + "</td><td>" + result[i].volumeInfo.authors + "</td><td>"+"<img src='"+result[i].volumeInfo.imageLinks.thumbnail+ "'>"+"</td><td>" + result[i].volumeInfo.description +"</td><td>"+"<button class='button' style = 'background-color: Transparent; border: none' id='"+result[i].id+"' onclick = 'changeStar(" +"\""+result[i].id+"\""+")'><i class='fa fa-star'style = 'color : gray'></i></button>"+"</td></tr>";

						$("#shelf").append(tmp);
					}
					$("#shelf").append("</tbody>");
				}
			});
		});

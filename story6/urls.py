from django.conf.urls import url, include
from .views import index, add_status, profil, story9, data_buku

#url for app
urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'add_status', add_status, name='add_status'),
    url(r'^profil', profil, name = 'profil'),
    url(r'^story9', story9, name = 'story9'),
    url(r'^buku', data_buku, name = 'data_buku'),
]
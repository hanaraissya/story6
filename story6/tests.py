from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index, add_status, profil, story9, data_buku
from .models import status
from .forms import status_Form
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

# Create your tests here.
class story6UnitTest(TestCase):
    
    def test_story6_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code,200)

    def test_story6_using_index_func(self):
      found = resolve('/')
      self.assertEqual(found.func, index)

    def test_model_can_fill_status(self):
    	new_status = status.objects.create(description='Hmm')
    	counting_all_available_status = status.objects.all().count()
    	self.assertEqual(counting_all_available_status, 1)

    def test_form_validation_for_blank_items(self):
      form = status_Form(data={'description': ''})
      self.assertFalse(form.is_valid())
      self.assertEqual(form.errors['description'], ["This field is required."] )

    def test_story6_post_success_and_render_the_result(self):
      test = 'Anonymous'
      response_post = Client().post('/add_status', {'description': test})
      self.assertEqual(response_post.status_code, 302)

      response= Client().get('/')
      html_response = response.content.decode('utf8')
      self.assertIn(test, html_response)

    def test_lab5_post_error_and_render_the_result(self):
      test = 'Anonymous'
      response_post = Client().post('/add_status', {'description': ''})
      self.assertEqual(response_post.status_code, 302)

      response= Client().get('/')
      html_response = response.content.decode('utf8')
      self.assertNotIn(test, html_response)

    def test_challenge6_url_is_exist(self):
      response = Client().get('/profil/')
      self.assertEqual(response.status_code,200)

    def test_challenge6_using_profil_func(self):
      found = resolve('/profil/')
      self.assertEqual(found.func, profil)

    def test_name_in_html(self):
      response = Client().get('/profil/')
      html_response = response.content.decode('utf8')
      self.assertIn("Hana Raissya", html_response)

    def test_story9_url_exists(self):
      response = Client().get('/story9/')
      self.assertEqual(response.status_code, 200)

    def test_json_data_buku_url_exists(self):
      response = Client().get('/buku/')
      self.assertEqual(response.status_code, 200)


# Create your tests here.
class story6FunctionalTest(TestCase):

  def setUp(self):
    chrome_options = Options()
    chrome_options.add_argument('--dns-prefetch-disable')
    chrome_options.add_argument('--no-sandbox')
    chrome_options.add_argument('--headless')
    chrome_options.add_argument('disable-gpu')
    service_log_path = "./chromedriver.log"
    service_args = ['--verbose']
    self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
    self.selenium.implicitly_wait(25)
    #self.browser = webdriver.Chrome()  
    super(story6FunctionalTest, self).setUp()

  def tearDown(self):
    self.selenium.quit()
    super(story6FunctionalTest, self).tearDown()

  def test_input_status(self):
    selenium = self.selenium
    # Opening the link we want to test
    selenium.get('http://127.0.0.1:8000/')

    # challenge story7 css index 
    css = selenium.find_element_by_css_selector('div.container')
    # challenge The profil.html link can be located like this:
    continue_link = selenium.find_element_by_link_text('Profil Saya')

    # find the form element
    description = selenium.find_element_by_id('id_description')

    submit = selenium.find_element_by_id('submit')

    # Fill the form with data
    description.send_keys('coba coba')


    # submitting the form
    submit.send_keys(Keys.RETURN)

#challenge story 7
  def test_in_profil(self):
    selenium = self.selenium
    # Opening the link we want to test
    selenium.get('http://127.0.0.1:8000/profil')
    # find the title
    self.assertIn('hanaaa', selenium.title)
    # header text in profil.html
    header_text = selenium.find_element_by_tag_name('h1').text
    self.assertIn('Profiles', header_text)
    # challenge story7 css profil
    content = selenium.find_element_by_css_selector('p.content')

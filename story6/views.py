from django.shortcuts import render
from django.http import HttpResponseRedirect, JsonResponse
from .models import status
from .forms import status_Form
import requests

# Create your views here.
response={}
API_BUKU = "https://www.googleapis.com/books/v1/volumes?q=quilting"
def index(request):
    response['stats'] = status_Form
    response['status'] = status.objects.all()
    html = 'index.html'
    return render(request, html, response)


def add_status(request):
    form = status_Form(request.POST or None)
    if(request.method == 'POST' and form.is_valid()):
        response['description'] = request.POST['description']
        a = status(description=response['description'])
        a.save()
        return HttpResponseRedirect('/')
    else:
        return HttpResponseRedirect('/')

def profil(request):
    return render(request, 'profil.html')

def story9(request):
    return render(request, 'story9.html',response)

def data_buku(request):
    data = requests.get(API_BUKU).json()
    return JsonResponse(data)
